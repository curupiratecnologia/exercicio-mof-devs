# Exercícios para Desenvolvedores
Segue abaixo as instruções para o desenvolvimento do Projeto


# Preparando o ambiente para o desenvolvimento

## Instalar o Node.js.
É necessário ter o Node.js instalado. Ele pode ser adquirido a partir da endereço: https://nodejs.org/en/download/

## Baixe o Projeto

O projeto pode ser baixado utilizando o git

```
git clone git@bitbucket.org:curupiratecnologia/exercicio-mof-devs.git 
```

## Crie um branch local
Crie um branch local com o seu nome
```
git checkout -b seu_nome
```

### Para iniciar o desenvolvimento

No diretório do projeto, digite:
Para instalar as dependências necessárias
```
npm install
```

Para iniciar o servidor
```
npm run serve
```
Agora basta acessar no seu navegador o endereço http://localhost:8080/ para ter acesso ao projeto.

As alterações feitas nos arquivos são refletidas automaticamente no navegador.




# Instruções sobre o exercício

O exercício consiste em criar uma página, onde é exibido em gráficos informações de orçamento de secretárias e de Estados.

O layout que deve ser feito está no arquivo layout.pdf. Prezamos pelo resultado final mais próximo do layout possível.

Para construção dessa página, deve ser utilizado o VUE.js. O projeto já possui todo o setup necessário para começar sua utilização. Basta criar/editar os componentes.

Para alterar a primeira página utilizem o arquivo src/App.vue. 
Criem os componentes necessários em src/components.


## Quanto aos dados a serem exibidos:

### Parte Secretárias
As informações de Secretarias podem ser adquiridas no endpoint http://mof.manatustecnologia.com.br//secretarias?ano_inicial=2015&ano_final=2018 onde pode ser determinado o ano inicial e final

As informações que são exibidas nas barras dos gráficos são o campo **dotacao**, e o que é % Executado é o campo **pago**

### Parte Estados
As informações de Secretarias podem ser adquiridas no endpoint http://mof.manatustecnologia.com.br//geral?ano_inicial=2018&ano_final=2018&esfera=estadual onde podem ser determinado o ano inicial e final

As informações que são exibidas nas barras dos gráficos são o campo **despesa total.dotacao**, e o que é % Executado é o campo **despesa total.pago**

## Enviando o exercício
### Adicione os arquivos que você criou
```
git add *
```

### Comite suas alterações
```
git commit -a
```

### Envie  suas alterações para o servidor
```
git push -u origin seu_nome
```

### Faça um Pull request
Faça o login no bitbucket, navegue até aba **branches** e faça um pull requests


















